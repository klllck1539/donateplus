﻿using DonatePlus.Services.Product;
using DonatePlus.Services.Supplier;
using DonatePlus.Web.Serialization;
using DonatePlus.Web.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace DonatePlus.Web.Controllers
{
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductService _productService;
        private readonly ISupplierService _supplierService;

        public ProductController(ILogger<ProductController> logger,
            IProductService productService,
            ISupplierService supplierService)
        {
            _logger = logger;
            _productService = productService;
            _supplierService = supplierService;
        }
        
        [HttpGet("/api/products")]
        public ActionResult GetProduct()
        {
            _logger.LogInformation("Getting all products");
            var products = _productService.GetAllProducts();
            var productViewModel = products.Select(p => ProductMapper.SerializeProductModel(p));
            return Ok(productViewModel);
        }

        [HttpPost("/api/product")]
        public ActionResult CreateNewProduct([FromBody] ProductViewModel productViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _logger.LogInformation("Create new product");
            productViewModel.CreatedOn = DateTime.UtcNow;
            productViewModel.UpdatedOn = DateTime.UtcNow;
            var newProduct = ProductMapper.SerializeProductModel(productViewModel);
            newProduct.Supplier = _supplierService.GetSupplierById(productViewModel.Supplier.Id);
            var newProductResponse =_productService.CreateProduct(newProduct);
            return Ok(newProductResponse);
        }

        [HttpPatch("/api/product/{id}")]
        public ActionResult ArchivedProduct(int id)
        {
            _logger.LogInformation("Archive product");
            var archiveResult = _productService.ArchiveProduct(id);
            return Ok(archiveResult);
        }
    }
}
