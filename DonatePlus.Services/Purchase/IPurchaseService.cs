﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DonatePlus.Services.Purchase
{
    public interface IPurchaseService
    {
        List<Data.Model.Purchase> GetPurchases();
        ServiceResponse<bool> GenerateInvoiceForPurchase(Data.Model.Purchase purchase);
        ServiceResponse<bool> MarkFulfilled(int id);
    }
}
