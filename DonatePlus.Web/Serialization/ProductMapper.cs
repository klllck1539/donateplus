﻿using DonatePlus.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DonatePlus.Web.Serialization
{
    public class ProductMapper
    {
        public static ProductViewModel SerializeProductModel(Data.Model.Product product)
        {
            return new ProductViewModel
            {
                Id = product.Id,
                CreatedOn = product.CreatedOn,
                UpdatedOn = product.UpdatedOn,
                Name = product.Name,
                Type = product.Type,
                Price = product.Price,
                IsAvailable = product.IsAvailable,
                Supplier = product.Supplier
            };
        }

        public static Data.Model.Product SerializeProductModel(ProductViewModel productViewModel)
        {
            return new Data.Model.Product
            {
                Id = productViewModel.Id,
                CreatedOn = productViewModel.CreatedOn,
                UpdatedOn = productViewModel.UpdatedOn,
                Name = productViewModel.Name,
                Type = productViewModel.Type,
                Price = productViewModel.Price,
                IsAvailable = true,
                Supplier = productViewModel.Supplier
            };
        }
    }
}
