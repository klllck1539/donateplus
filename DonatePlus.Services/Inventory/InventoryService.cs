﻿using DonatePlus.Data;
using DonatePlus.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DonatePlus.Services.Inventory
{
    public class InventoryService : IInventoryService
    {
        private readonly DonateDbContext _context;

        public InventoryService(DonateDbContext context)
        {
            _context = context;
        }

        public List<ProductInventory> GetCurrentInventory()
        {
            return _context.ProductInventories
                .Include(pi => pi.Product).ThenInclude(p => p.Supplier)
                .Where(pi => pi.Product.IsAvailable)
                .ToList();
        }

        public ProductInventory GetInventoryByProductId(int productId)
        {
            return _context.ProductInventories
                .Include(pi => pi.Product)
                .FirstOrDefault(pi => pi.Product.Id == productId);
        }

        public ServiceResponse<ProductInventory> UpdateAvailableProducts(int id, int amount)
        {
            var dateNow = DateTime.UtcNow;
            try
            {
                var inventory = _context.ProductInventories
                    .Include(pi => pi.Product)
                    .First(pi => pi.Product.Id == id);

                inventory.QuantityOnHand += amount;

                try
                {
                    CreateSnapshot();
                }
                catch (Exception) { }

                _context.SaveChanges();

                return new ServiceResponse<ProductInventory>
                {
                    IsSuccess = true,
                    Data = inventory,
                    Message = "Product recive to inventory",
                    Time = dateNow
                };
            }
            catch (Exception)
            {
                return new ServiceResponse<ProductInventory>
                {
                    IsSuccess = false,
                    Data = null,
                    Message = "Fail to update product inventory",
                    Time = dateNow
                };
            }
        }

        public List<ProductInventorySnapshot> GetSnapshotHistory()
        {
            var earliest = DateTime.UtcNow - TimeSpan.FromHours(2);

            return _context.ProductInventorySnapshots
                .Include(snap => snap.Product)
                .Where(snap => snap.SnapshotTime > earliest && snap.Product.IsAvailable)
                .ToList();
        }

        private void CreateSnapshot()
        {
            var dateNow = DateTime.UtcNow;

            var inventories = _context.ProductInventories
                .Include(pi => pi.Product)
                .OrderBy(pi => pi.Product.Name)
                .Take(5)
                .ToList();

            foreach (var inv in inventories)
            {
                var snapshot = new ProductInventorySnapshot
                {
                    SnapshotTime = dateNow,
                    Product = inv.Product,
                    QuantityOnHand = inv.QuantityOnHand
                };

                _context.Add(snapshot);

            }
        }
    }
}
