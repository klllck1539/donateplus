﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DonatePlus.Data.Migrations
{
    public partial class changesuppliermodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SupplierAddressId",
                table: "Suppliers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_SupplierAddressId",
                table: "Suppliers",
                column: "SupplierAddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_Suppliers_SupplierAddresses_SupplierAddressId",
                table: "Suppliers",
                column: "SupplierAddressId",
                principalTable: "SupplierAddresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Suppliers_SupplierAddresses_SupplierAddressId",
                table: "Suppliers");

            migrationBuilder.DropIndex(
                name: "IX_Suppliers_SupplierAddressId",
                table: "Suppliers");

            migrationBuilder.DropColumn(
                name: "SupplierAddressId",
                table: "Suppliers");
        }
    }
}
