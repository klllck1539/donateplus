﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DonatePlus.Data.Model
{
    public class PurchaseItem
    {
        public int Id { get; set; }
        public int Quantity { get; set; }

        public Product Product { get; set; }
    }
}
