﻿using DonatePlus.Data.Model;
using System;

namespace DonatePlus.Web.ViewModel
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public string Name { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public bool IsAvailable { get; set; }
        public Supplier Supplier { get; set; }
    }
}
