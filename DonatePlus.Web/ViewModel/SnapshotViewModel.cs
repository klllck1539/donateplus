﻿using System;
using System.Collections.Generic;

namespace DonatePlus.Web.ViewModel
{
    public class SnapshotViewModel
    {
        public List<int> QuantityOnHand { get; set; }
        public int ProductId { get; set; }
    }

    public class SnapshotResponse
    {
        public List<SnapshotViewModel> ProductInventorySnapshots { get; set; }
        public List<DateTime> Timeline { get; set; }
    }
}
