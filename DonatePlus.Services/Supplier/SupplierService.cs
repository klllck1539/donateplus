﻿using DonatePlus.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DonatePlus.Services.Supplier
{
    public class SupplierService : ISupplierService
    {
        private readonly DonateDbContext _context;

        public SupplierService(DonateDbContext context)
        {
            _context = context;
        }

        public List<Data.Model.Supplier> GetAllSuppliers()
        {
            return _context.Suppliers
                .Include(s => s.SupplierAddress)
                .OrderBy(s => s.Name)
                .ToList();
        }

        public Data.Model.Supplier GetSupplierById(int id)
        {
            return _context.Suppliers.Find(id);
        }

        public ServiceResponse<Data.Model.Supplier> CreateSupplier(Data.Model.Supplier supplier)
        {
            try
            {
                _context.Add(supplier);
                _context.SaveChanges();

                return new ServiceResponse<Data.Model.Supplier>
                {
                    IsSuccess = true,
                    Message = "New supplier has been added",
                    Time = DateTime.UtcNow,
                    Data = supplier
                };
            }
            catch (Exception e)
            {
                return new ServiceResponse<Data.Model.Supplier>
                {
                    IsSuccess = false,
                    Message = e.StackTrace,
                    Time = DateTime.UtcNow,
                    Data = supplier
                };
            }
        }

        public ServiceResponse<bool> DeleteSupplier(int id)
        {
            var dateNow = DateTime.UtcNow;
            var supplier = _context.Suppliers.Find(id);
            var supplierAddress = _context.SupplierAddresses
                .First(sa => sa.Id == supplier.SupplierAddress.Id);

            if (supplier == null)
            {
                return new ServiceResponse<bool>
                {
                    Time = dateNow,
                    IsSuccess = false,
                    Message = "Supplier not found",
                    Data = false
                };
            }

            try
            {
                _context.Suppliers.Remove(supplier);
                _context.SupplierAddresses.Remove(supplierAddress);
                _context.SaveChanges();

                return new ServiceResponse<bool>
                {
                    Time = dateNow,
                    IsSuccess = true,
                    Message = "Supplier has been deleted",
                    Data = true
                };
            }
            catch (Exception e)
            {
                return new ServiceResponse<bool>
                {
                    Time = dateNow,
                    IsSuccess = false,
                    Message = e.StackTrace,
                    Data = false
                };
            }
        }
    }
}
