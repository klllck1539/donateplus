import { IProduct } from "@/types/Product";

export interface IPurchase {
    supplierId: number;
    purchaseItems: IPurchaseItem[];
    createdOn?: Date;
    updatedOn?: Date;
}

export interface IPurchaseItem {
    product?: IProduct;
    quantity: number;
}