import axios from 'axios'
import { IProductInventory } from '@/types/Product'
import { IInventoryTimeLine } from '@/types/InventoryGraph'
import { IShipment } from '@/types/Shipment'

export class InventoryService {
    API_URL = process.env.VUE_APP_API_URL;

    public async getProducts() : Promise<IProductInventory[]> {
        const result = await axios.get(`${this.API_URL}/inventory/`);
        return result.data;
    }

    public async updateInventoryQuantity(shipment: IShipment) {
        const result = await axios.patch(`${this.API_URL}/inventory`, shipment);
        return result.data;
    }

    public async getSnapshotHistory(): Promise<IInventoryTimeLine> {
        const result: any = await axios.get(`${this.API_URL}/inventory/snapshot`);
        return result.data;
    }
}
