﻿using DonatePlus.Services.Purchase;
using DonatePlus.Services.Supplier;
using DonatePlus.Web.Serialization;
using DonatePlus.Web.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DonatePlus.Web.Controllers
{
    [ApiController]
    public class PurchaseController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IPurchaseService _purchaseService;
        private readonly ISupplierService _supplierService;

        public PurchaseController(ILogger<ProductController> logger,
            IPurchaseService purchaseService,
            ISupplierService supplierService)
        {
            _logger = logger;
            _purchaseService = purchaseService;
            _supplierService = supplierService;
        }

        [HttpPost("/api/purchase")]
        public ActionResult GenerateNewPurchase([FromBody] InvoiceViewModel invoiceViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _logger.LogInformation("Generate purchase's invoice");
            var purchase = PurchaseMapper.SerializeInvoiceToPurchase(invoiceViewModel);
            purchase.Supplier = _supplierService.GetSupplierById(invoiceViewModel.SupplierId);
            _purchaseService.GenerateInvoiceForPurchase(purchase);
            return Ok();
        }

        [HttpGet("/api/purchases")]
        public ActionResult GetPurchases()
        {
            _logger.LogInformation("Get all purchases");
            var purchases = _purchaseService.GetPurchases();
            var purchaseModels = PurchaseMapper.SerializePurchasesToViewModels(purchases);
            return Ok(purchaseModels);
        }

        [HttpPatch("/api/purchase/complete/{id}")]
        public ActionResult MarkPurchaseComplete(int id)
        {
            _logger.LogInformation($"Make purchase {id} complete...");
            _purchaseService.MarkFulfilled(id);
            return Ok();
        }
    }
}
