﻿using DonatePlus.Services.Inventory;
using DonatePlus.Web.Serialization;
using DonatePlus.Web.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace DonatePlus.Web.Controllers
{
    [ApiController]
    public class InventoryController : ControllerBase
    {
        private readonly ILogger<InventoryController> _logger;
        private readonly IInventoryService _inventoryService;

        public InventoryController(ILogger<InventoryController> logger,
            IInventoryService inventoryService)
        {
            _logger = logger;
            _inventoryService = inventoryService;
        }

        [HttpGet("/api/inventory")]
        public ActionResult GetCurrentInventory()
        {
            _logger.LogInformation("Get current inventory");
            var inventory = _inventoryService.GetCurrentInventory()
                .Select(pi => new ProductInventoryViewModel
                {
                    Id = pi.Id,
                    QuantityOnHand = pi.QuantityOnHand,
                    IdealQuantity = pi.IdealQuantity,
                    Product = ProductMapper.SerializeProductModel(pi.Product)
                })
                .OrderBy(inventory => inventory.Product.Name)
                .ToList();

            return Ok(inventory);
        }

        [HttpPatch("/api/inventory")]
        public ActionResult UpdateInventory([FromBody] ShipmentViewModel shipment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _logger.LogInformation($"Update inventory for {shipment.ProductId}");
            var productId = shipment.ProductId;
            var productAmount = shipment.Amount;
            var inventory = _inventoryService.UpdateAvailableProducts(productId, productAmount);
            return Ok(inventory);
        }

        [HttpGet("/api/inventory/snapshot")]
        public ActionResult GetSnapshotHistory()
        {
            _logger.LogInformation("Get snapshot history");
            try
            {
                var snapshotHistory = _inventoryService.GetSnapshotHistory();
                var timelineMarkers = snapshotHistory
                        .Select(tl => tl.SnapshotTime)
                        .Distinct()
                        .ToList();

                var snapshots = snapshotHistory
                    .GroupBy(history => history.Product, history => history.QuantityOnHand,
                        (key, q) => new SnapshotViewModel { 
                            ProductId = key.Id,
                            QuantityOnHand = q.ToList()
                        })
                    .OrderBy(history => history.ProductId)
                    .ToList();

                var snapshotViewModel = new SnapshotResponse
                {
                    ProductInventorySnapshots = snapshots,
                    Timeline = timelineMarkers
                };

                return Ok(snapshotViewModel);
            }
            catch (Exception e)
            {
                _logger.LogError("Error: can not getting snapshot history...");
                return BadRequest(e.StackTrace);
            }
        }
    }
}
