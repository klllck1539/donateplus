﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DonatePlus.Data.Migrations
{
    public partial class Addratingfieldforsuppliers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Rating",
                table: "Suppliers",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rating",
                table: "Suppliers");
        }
    }
}
