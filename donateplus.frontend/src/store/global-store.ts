import { make } from "vuex-pathify";

import { IInventoryTimeLine } from "@/types/GraphTypes"
import { InventoryService } from "@/services/inventory-service"

class GlobalStore {
    snapshotTimeLine: IInventoryTimeLine = {
        productInventorySnapshots: [],
        timeline: []
    }
    isTimeLineBuild = false;
}

const state = new GlobalStore();
const mutations = make.mutations(state);

const actions = {
    async assignSnapshots({ commit }) {
        const inventoryService = new InventoryService();
        const result = await inventoryService.getSnapshotHistory();

        const timeline: IInventoryTimeLine = {
            productInventorySnapshots: result.productInventorySnapshots,

            timeline: result.timeline
        }

        commit('SET_SNAPSHOT_TIME_LINE', timeline);
        commit('SET_IS_TIME_LINE_BUILD', true);
    },
};

const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}