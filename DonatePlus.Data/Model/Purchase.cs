﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DonatePlus.Data.Model
{
    public class Purchase
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public Supplier Supplier { get; set; }
        public List<PurchaseItem> PurchaseItems { get; set; }
        public bool IsPaid { get; set; }
    }
}
