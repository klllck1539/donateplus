﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DonatePlus.Web.ViewModel
{
    public class PurchaseViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public SupplierViewModel Supplier { get; set; }
        public List<PurchaseItemViewModel> PurchaseItems { get; set; }
        public bool IsPaid { get; set; }
    }
}
