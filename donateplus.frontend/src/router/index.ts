import Vue from "vue";
import VueRouter from "vue-router";
import Purchases from "@/views/Purchases.vue";
import Products from "@/views/Products.vue";
import Suppliers from "@/views/Suppliers.vue";
import CreateInvoice from "@/views/CreateInvoice.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Products
  },
  {
    path: '/products',
    name: 'products',
    component: Products
  }, 
  {
    path: '/suppliers',
    name: 'suppliers',
    component: Suppliers
  },
  {
    path: '/invoice/new',
    name: 'create-invoice',
    component: CreateInvoice
  },
  {
    path: '/purchases',
    name: 'purchases',
    component: Purchases
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
