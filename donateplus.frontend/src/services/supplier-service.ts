import axios from 'axios'
import { ISupplier } from '@/types/Supplier'
import { IServiceResponse } from '@/types/ServiceResponse'

export class SupplierService {
    API_URL = process.env.VUE_APP_API_URL;

    public async getSuppliers(): Promise<ISupplier[]> {
        const result: any = await axios.get(`${this.API_URL}/suppliers/`);
        return result.data;
    }

    public async addSupplier(newSupplier: ISupplier): Promise<IServiceResponse<ISupplier[]>> {
        const result: any = await axios.post(`${this.API_URL}/supplier/`, newSupplier);
        return result.data;
    }

    public async deleteSupplier(supplierId: number): Promise<boolean> {
        const result: any = await axios.delete(`${this.API_URL}/supplier/${supplierId}`);
        return result.data;
    }
}