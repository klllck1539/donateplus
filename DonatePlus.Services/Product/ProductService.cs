﻿using DonatePlus.Data;
using DonatePlus.Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DonatePlus.Services.Product
{
    public class ProductService : IProductService
    {
        private readonly DonateDbContext _context;

        public ProductService(DonateDbContext context)
        {
            _context = context; 
        }

        public List<Data.Model.Product> GetAllProducts()
        {
            return _context.Products.Include(p => p.Supplier).ToList();
        }

        public Data.Model.Product GetProductById(int id)
        {
            return _context.Products.Find(id);
        }

        public ServiceResponse<Data.Model.Product> CreateProduct(Data.Model.Product product)
        {
            try
            {
                _context.Products.Add(product);

                var newInventory = new ProductInventory
                {
                    Product = product,
                    CreatedOn = DateTime.UtcNow,
                    UpdatedOn = DateTime.UtcNow,
                    QuantityOnHand = 1,
                    IdealQuantity = 10
                };

                _context.ProductInventories.Add(newInventory);

                _context.SaveChanges();

                return new ServiceResponse<Data.Model.Product>
                {
                    Data = product,
                    Time = DateTime.UtcNow,
                    Message = "Save new product",
                    IsSuccess = true
                };
            }
            catch (Exception e)
            {
                return new ServiceResponse<Data.Model.Product>
                {
                    Data = product,
                    Time = DateTime.UtcNow,
                    Message = e.StackTrace,
                    IsSuccess = false
                };
            }
        }

        public ServiceResponse<Data.Model.Product> ArchiveProduct(int id)
        {
            try
            {
                var product = _context.Products.Find(id);
                product.IsAvailable = false;
                _context.SaveChanges();

                return new ServiceResponse<Data.Model.Product>
                {
                    Data = product,
                    Time = DateTime.UtcNow,
                    Message = "Archived product successfull",
                    IsSuccess = true
                };
            }
            catch (Exception e)
            {
                return new ServiceResponse<Data.Model.Product>
                {
                    Data = null,
                    Time = DateTime.UtcNow,
                    Message = e.StackTrace,
                    IsSuccess = false
                };
            }
        }
    }
}
