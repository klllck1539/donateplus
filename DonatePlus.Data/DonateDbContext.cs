﻿using DonatePlus.Data.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DonatePlus.Data
{
    public class DonateDbContext : IdentityDbContext
    {
        public DonateDbContext() { }
        public DonateDbContext(DbContextOptions options)
            : base(options) { }

        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<SupplierAddresses> SupplierAddresses { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductInventory> ProductInventories { get; set; }
        public virtual DbSet<ProductInventorySnapshot> ProductInventorySnapshots { get; set; }
        public virtual DbSet<Purchase> Purchases { get; set; }
        public virtual DbSet<PurchaseItem> PurchaseItems { get; set; }
    }
}
