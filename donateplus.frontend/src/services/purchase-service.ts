import axios from 'axios'
import { IPurchase } from '@/types/Purchase'

export class PurchaseService {
    API_URL = process.env.VUE_APP_API_URL;

    public async getPurchases(): Promise<any> {
        const result: any = await axios.get(`${this.API_URL}/purchases/`);
        return result.data;
    }

    public async makeNewPurchase(purchase: IPurchase): Promise<any> {
        const now = new Date();
        purchase.createdOn = now;
        purchase.updatedOn = now;
        const result: any = await axios.post(`${this.API_URL}/purchase/`, purchase);
        return result.data; 
    }

    public async markPurchaseComplete(purchaseId: number): Promise<any> {
        const result: any = await axios.patch(`${this.API_URL}/purchase/complete/${purchaseId}`);
        return result.data; 
    }
}