import { IPurchaseItem } from "./Purchase";
import { ISupplier } from "./Supplier";

export interface ISales {
    id: number;
    createdOn: Date;
    updatedOn?: Date;
    isPaid: boolean;
    supplier: ISupplier;
    purchaseItems: IPurchaseItem[];
}