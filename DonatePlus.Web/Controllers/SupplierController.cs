﻿using DonatePlus.Services.Supplier;
using DonatePlus.Web.Serialization;
using DonatePlus.Web.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace DonatePlus.Web.Controllers
{
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly ILogger<SupplierController> _logger;
        private readonly ISupplierService _supplierService;

        public SupplierController(ILogger<SupplierController> logger,
            ISupplierService supplierService)
        {
            _logger = logger;
            _supplierService = supplierService;
        }

        [HttpGet("/api/suppliers")]
        public ActionResult GetSuppliers()
        {
            _logger.LogInformation("Get all suppliers from db");
            var suppliers = _supplierService.GetAllSuppliers();

            var supplierModels = suppliers.Select(s => new SupplierViewModel
            {
                Id = s.Id,
                Name = s.Name,
                CreatedOn = s.CreatedOn,
                UpdatedOn = s.UpdatedOn,
                Description = s.Description,
                Rating = s.Rating,
                Type = s.Type,
                SupplierAddress = SupplierMapper.MapSupplierAddress(s.SupplierAddress)
            })
            .OrderBy(s => s.Type)
            .ToList();

            return Ok(supplierModels);
        }

        [HttpPost("/api/supplier")]
        public ActionResult CreateSupplier([FromBody] SupplierViewModel supplierViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _logger.LogInformation("Add new supplier in db");
            supplierViewModel.CreatedOn = DateTime.UtcNow;
            supplierViewModel.UpdatedOn = DateTime.UtcNow;
            var supplierData = SupplierMapper.SerializeSupplier(supplierViewModel);
            var newSupplier = _supplierService.CreateSupplier(supplierData);
            return Ok(newSupplier);
        }

        [HttpDelete("/api/supplier/{id}")]
        public ActionResult DeleteSupplier(int id)
        {
            _logger.LogInformation($"Delete a supplier by {id} from db");
            var result = _supplierService.DeleteSupplier(id);
            return Ok(result);
        }
    }
}
