﻿using DonatePlus.Data.Model;
using DonatePlus.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DonatePlus.Web.Serialization
{
    public class PurchaseMapper
    {
        public static Purchase SerializeInvoiceToPurchase(InvoiceViewModel invoiceViewModel)
        {
            var purchaseItems = invoiceViewModel.PurchaseItems
                .Select(item => new PurchaseItem
                {
                    Id = item.Id,
                    Quantity = item.Quantity,
                    Product = ProductMapper.SerializeProductModel(item.Product)
                }).ToList();

            return new Purchase
            {
                PurchaseItems = purchaseItems,
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                IsPaid = false,
            };
        }

        public static List<PurchaseViewModel> SerializePurchasesToViewModels(IEnumerable<Purchase> purchases)
        {
            return purchases.Select(purchase => new PurchaseViewModel
            {
                Id = purchase.Id,
                CreatedOn = purchase.CreatedOn,
                UpdatedOn = purchase.UpdatedOn,
                IsPaid = purchase.IsPaid,
                PurchaseItems = SerializePurchaseItems(purchase.PurchaseItems),
                Supplier = SupplierMapper.SerializeSupplier(purchase.Supplier)    
            }).ToList();
        }

        private static List<PurchaseItemViewModel> SerializePurchaseItems(IEnumerable<PurchaseItem> purchaseItems)
        {
            return purchaseItems.Select(item => new PurchaseItemViewModel
            {
                Id = item.Id,
                Quantity = item.Quantity,
                Product = ProductMapper.SerializeProductModel(item.Product)
            }).ToList();
        }
    }
}
