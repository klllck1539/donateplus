﻿using DonatePlus.Data.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DonatePlus.Services.Inventory
{
    public interface IInventoryService
    {
        List<ProductInventory> GetCurrentInventory();
        ServiceResponse<ProductInventory> UpdateAvailableProducts(int id, int amount);
        ProductInventory GetInventoryByProductId(int productId);
        List<ProductInventorySnapshot> GetSnapshotHistory();
    }
}
