﻿using DonatePlus.Data.Model;
using System.Collections.Generic;

namespace DonatePlus.Services.Product
{
    public interface IProductService
    {
        public List<Data.Model.Product> GetAllProducts();
        public Data.Model.Product GetProductById(int id);
        public ServiceResponse<Data.Model.Product> CreateProduct(Data.Model.Product product);
        public ServiceResponse<Data.Model.Product> ArchiveProduct(int id);
    }
}
