import ISupplier from '@/types/Supplier'

export interface IProduct {
    id: number;
    name: string;
    type: string;
    price: number;
    supplier: ISupplier;
}

export interface IProductInventory {
    id: number;
    product: IProduct;
    quantityOnHand: number;
    idealQuantity: number;
}