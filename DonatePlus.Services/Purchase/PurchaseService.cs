﻿using DonatePlus.Data;
using DonatePlus.Services.Inventory;
using DonatePlus.Services.Product;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DonatePlus.Services.Purchase
{
    public class PurchaseService : IPurchaseService
    {
        private readonly DonateDbContext _context;
        private readonly IProductService _productService;
        private readonly IInventoryService _inventoryService;

        public PurchaseService(DonateDbContext context,
            IProductService productService,
            IInventoryService inventoryService)
        {
            _context = context;
            _productService = productService;
            _inventoryService = inventoryService;
        }

        public List<Data.Model.Purchase> GetPurchases()
        {
            return _context.Purchases
                .Include(prch => prch.Supplier)
                    .ThenInclude(s => s.SupplierAddress)
                .Include(prch => prch.PurchaseItems)
                    .ThenInclude(p => p.Product)
                .ToList();
        }

        public ServiceResponse<bool> GenerateInvoiceForPurchase(Data.Model.Purchase purchase)
        {
            foreach (var item in purchase.PurchaseItems)
            {
                item.Product = _productService
                    .GetProductById(item.Product.Id);

                var inventoryId = _inventoryService
                    .GetInventoryByProductId(item.Product.Id).Id;

                _inventoryService
                    .UpdateAvailableProducts(inventoryId, -item.Quantity);
            }

            try
            {
                _context.Purchases.Add(purchase);
                _context.SaveChanges();

                return new ServiceResponse<bool>
                {
                    IsSuccess = true,
                    Data = true,
                    Message = "Purchase has been created",
                    Time = DateTime.UtcNow
                };
            }
            catch (Exception e)
            {
                return new ServiceResponse<bool>
                {
                    IsSuccess = false,
                    Data = false,
                    Message = e.StackTrace,
                    Time = DateTime.UtcNow
                };
            }
        }

        public ServiceResponse<bool> MarkFulfilled(int id)
        {
            var dateNow = DateTime.UtcNow;
            var purchase = _context.Purchases.Find(id);
            purchase.UpdatedOn = dateNow;
            purchase.IsPaid = true;

            try
            {
                _context.Purchases.Update(purchase);
                _context.SaveChanges();

                return new ServiceResponse<bool>
                {
                    IsSuccess = true,
                    Data = true,
                    Message = $"Purchase {purchase.Id} paid and close",
                    Time = dateNow
                };
            }
            catch (Exception e)
            {
                return new ServiceResponse<bool>
                {
                    IsSuccess = false,
                    Data = false,
                    Message = e.StackTrace,
                    Time = DateTime.UtcNow
                };
            }
        }
    }
}
