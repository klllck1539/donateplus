﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DonatePlus.Data.Model
{
    public class Product
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        [MaxLength(64)]
        public string Name { get; set; }
        [MaxLength(24)]
        public string Type { get; set; }
        public decimal Price { get; set; }
        public bool IsAvailable { get; set; }

        public Supplier Supplier { get; set; }
    }
}
