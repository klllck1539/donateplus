
export interface ISupplier {
    id: number;
    createdOn: Date;
    updatedOn?: Date;
    name: string;
    description: string;
    type: string;
    rating: number;
    supplierAddress: ISupplierAddress;
}

export interface ISupplierAddress {
    id: number;
    createdOn: Date;
    updatedOn?: Date;
    country: string;
    city: string;
    addressLine: string;
    postalCode: string;
}

export interface ISupplierOptions {
    labels: string[]
}