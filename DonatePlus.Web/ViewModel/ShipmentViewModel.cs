﻿namespace DonatePlus.Web.ViewModel
{
    public class ShipmentViewModel
    {
        public int ProductId { get; set; }
        public int Amount { get; set; }
    }
}
