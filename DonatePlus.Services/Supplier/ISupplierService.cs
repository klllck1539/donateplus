﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DonatePlus.Services.Supplier
{
    public interface ISupplierService
    {
        List<Data.Model.Supplier> GetAllSuppliers();
        Data.Model.Supplier GetSupplierById(int id);
        ServiceResponse<Data.Model.Supplier> CreateSupplier(Data.Model.Supplier supplier);
        ServiceResponse<bool> DeleteSupplier(int id);
    }
}
