﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DonatePlus.Data.Model
{
    public class Supplier
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public double Rating { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public SupplierAddresses SupplierAddress { get; set; }
    }
}
