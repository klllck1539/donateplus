export interface IShipment {
    productId: number;
    amount: number;
}