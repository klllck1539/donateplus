﻿using DonatePlus.Data.Model;
using DonatePlus.Web.ViewModel;
using System;

namespace DonatePlus.Web.Serialization
{
    public static class SupplierMapper
    {
        public static SupplierViewModel SerializeSupplier(Supplier supplier)
        {
            return new SupplierViewModel
            {
                Id = supplier.Id,
                CreatedOn = supplier.CreatedOn,
                UpdatedOn = supplier.UpdatedOn,
                Name = supplier.Name,
                Description = supplier.Description,
                Type = supplier.Type,
                Rating = supplier.Rating,
                SupplierAddress = MapSupplierAddress(supplier.SupplierAddress)
            };
        }

        public static Supplier SerializeSupplier(SupplierViewModel supplierViewModel)
        {
            return new Supplier
            {
                Id = supplierViewModel.Id,
                CreatedOn = supplierViewModel.CreatedOn,
                UpdatedOn = supplierViewModel.UpdatedOn,
                Name = supplierViewModel.Name,
                Description = supplierViewModel.Description,
                Type = supplierViewModel.Type,
                Rating = supplierViewModel.Rating,
                SupplierAddress = MapSupplierAddress(supplierViewModel.SupplierAddress)
            };
        }

        public static SupplierAddressViewModel MapSupplierAddress(SupplierAddresses address)
        {
            return new SupplierAddressViewModel
            {
                Id = address.Id,
                CreateOn = address.CreatedOn,
                UpdateOn = address.UpdatedOn,
                AddressLine = address.AddressLine,
                City = address.City,
                Country = address.Country,
                PostalCode = address.PostalCode,
            };
        }

        public static SupplierAddresses MapSupplierAddress(SupplierAddressViewModel address)
        {
            return new SupplierAddresses
            {
                Id = address.Id,
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                AddressLine = address.AddressLine,
                City = address.City,
                Country = address.Country,
                PostalCode = address.PostalCode,
            };
        }
    }
}
